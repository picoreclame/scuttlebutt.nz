---
title: about
# this follows styles in layouts > page > single
type: page
---

# About

**Scuttlebutt is a decentralised secure gossip platform.**

Like other social platforms, you can send messages to your friends and share posts onto a feed. The cool thing is that the underlying technology here means that messages are passed directly between friends via a peer-to-peer (p2p) [gossip protocol](https://en.wikipedia.org/wiki/Gossip_protocol).

As a decentralized social network, Scuttlebutt passes the data from friend to friend, without any central server. The data is localised and distributed so it also happens to work offline!

The name, Scuttlebutt, came from sea-slang for gossip. Basically, like a watercooler on a ship, where sailors and pirates go have a yarn.

To learn more check out our [talks](/docs/talks), [articles](/docs/media), [documentation](/docs) or [just get started](/get-started)!

![dancing hermie](/images/gif/large-hermies-dancing.gif)

&nbsp;

&nbsp;
